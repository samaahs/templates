# How to create an md file

# Objectives
1. Adding Headings and Titles
2. Adding steps, edits and notes
3. Adding images
4. Adding codeblocks
5. Adding Changelog

# Adding Headings and Titles

1. There are multiple sizes in which you can add headings and titles to a md file. We represent a heading or title with ***#***

2. For example:
To display the headings and titles in different sizes, we use: 

# Heading 1
## Heading 2
### Heading 3

3. The code for the same would be

```
# Heading 1
## Heading 2
### Heading 3
```
{:codeblock}


4. We usually use the `# heading 1` to mention and highlight major topics, `# heading 2` and `# heading 3` for sub-topics. Make sure to have space after # and the title

# Adding steps, edits and notes

## Writing steps
1. For writing the steps, it is basic the numbering followed by the text
```
1. This is step 1
2. This is step 2
```
{:codeblock}

2. `br` tag is used to break the line

```
This is step 1<br>This is step 2
```
{:codeblock}

3. Output for the same would be:<br>
This is step 1<br>This is step 2

## Editing the steps/specific word
1. Bold & italics are usually used with `*` 

2. For italics, we wrap the work/statement with *, ** for bold, and *** for emphasizing.

3. We also use *``* to highlight a specific word like `this`

  For example:

```
this is **bold** <br>
this is *italics* <br>
this text is ***emphasized*** <br>
focus on `this`
```
{:codeblock}

Output would be as follows: <br>
this is **bold** <br>
this is *italics* <br>
this text is ***emphasized*** <br>
focus on `this`

## Adding notes
1. To add a note which looks like this:
> **Note:** This is an additional instruction to consider

The syntax will be:

```
> **Note:** This is an additional instruction to consider
```
{:codeblock}

# Adding images

1. To add an image, we use an image tag.

2. Syntax for image tag:
```
<img src="FILE_PATH"/>
```
{:codeblock}

3. Widht & Height are optional attributes that can be used to resize the image 
```
width="200" height="200"

<img src="FILE_PATH" width="200" height="200"/>
```
{:codeblock}

# Adding codeblocks

1. Codeblocks are usually used while we want to give the code to the learner for them to copy.

2. It creates a seperate block with the code within something like this:

```
Enter your code
```
{:codeblock}

3. Syntax:
<img src="images/codeblock.png"/>

# Adding Changelog

1. Logs are added in the end of the file to display who edited the file. Along with the name, date of edit and some comments on what was edited in a single line.

Syntax:
```
## Changelog
| Date | Version | Changed by | Change Description |
|------|--------|--------|---------|
| 28-April-2022 | 1.0 | Samaah | Initial version created |
|   |   |   |   |
```
{:codeblock}

It will look something like this:

## Changelog
| Date | Version | Changed by | Change Description |
|------|--------|--------|---------|
| 28-April-2022 | 1.0 | Samaah | Initial version created |
|   |   |   |   |
