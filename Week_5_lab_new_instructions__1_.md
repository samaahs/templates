## Kubernetes 20.07.22 Week5 new lab instructions


### Week 1 revision

1. 
`
git clone https://github.com/Sklup55/Kubernetes_20.07.22.git
`

2. 
`
cd guestbook_20.07.22/
`

3.
`
cd labs
`

4.
`
cd 1_ContainersAndDocker/
`

5.
`
docker images
`

6.
`
docker pull hello-world
`

7. 
`
docker run hello-world
`

8.
`
docker ps -a
`

9.
`
docker container rm <container_id>
`

10.
`
docker ps -a
`

11.
`
docker build . -t myimage:v1
`
12.

`
docker images
`

13.
`
docker run -p 8080:8080 myimage:v1
`

14. Split terminal

In 2nd terminal
15.
`
curl localhost:8080
`

16.
`
docker stop $(docker ps -q)
`

17.
`
docker ps
`

18.
`
exit
`

19.
`
ibmcloud target
`

20.

`
ibmcloud cr namespaces
`

21.
`
ibmcloud cr region-set us-south
`

22.
`
ibmcloud cr login
`

23.

`
export MY_NAMESPACE=sn-labs-$USERNAME
`

24.
`
docker tag myimage:v1 us.icr.io/$MY_NAMESPACE/hello-world:1
`

25.
`
docker push us.icr.io/$MY_NAMESPACE/hello-world:1
`

26.
`
ibmcloud cr images
`

### Build guestbook (existing Week 5)

1.
`
/home/project/Kubernetes_20.07.22/guestbook_20.07.22/v1/guestbook
`

2.
`
export MY_NAMESPACE=sn-labs-$USERNAME
`

3.
`
docker build . -t us.icr.io/$MY_NAMESPACE/guestbook:v1
`

4.
`
docker push us.icr.io/$MY_NAMESPACE/guestbook:v1
`

5.
`
ibmcloud cr images
`

### Create pod with declarative command (from Week3)

1. 
`
kubectl apply -f guestbook_20.07.22.yaml
`

2.
`
kubectl get deployments
`

3.
`
kubectl get pods
`

### Load balancing (from Week3)

1.
`
kubectl expose deployment/guestbook

`

2.
`
kubectl get services
`

3. Open new terminal & type
`
kubectl proxy
`


In the 1st terminal
------ Having errors with curl commands
4.
`
curl -L localhost:8001/api/v1/namespaces/sn-labs-$USERNAME/services/guestbook/proxy
`

5.
`
for i in `seq 10`; do curl -L localhost:8001/api/v1/namespaces/sn-labs-$USERNAME/services/guestbook/proxy; done

`
6. Stop proxy with CTRL + C


### Creating HPA with kubectl commands (New)

1.
`
kubectl apply -f guestbook_20.07.22.yaml
`

2.
`
kubectl autoscale deployment guestbook --cpu-percent=50 --min=1 --max=10
`

3. In a new terminal type:
`
kubectl run -i --tty load-generator --rm --image=busybox:1.28 --restart=Never -- /bin/sh -c "while sleep 0.01; do wget -q -O- http://guestbook; done"
`

4. In the 1st terminal
`
kubectl get hpa guestbook -w
`
>Note: Wait for 10 mins for the replicas to get scaled to 5 or more

5. To view the increase no of replicas
`
kubectl get deployment guestbook
`

6. Stop proxy in 2nd terminal with CTRL+C.

Type the below commands in the 1st terminal after 10 mins:

7. 
`
kubectl get hpa guestbook
`

8.
`
kubectl get deployment guestbook

`
9.
`
kubectl delete deployment guestbook
`

10.
`
kubectl delete service guestbook
`

11.
`
kubectl delete hpa guestbook
`

### Rolling updates (from week 3)

1. Change line 7 in app.js to:
`
  res.send('Hello world from ' + hostname + '! Your app has been updated!\n')
`

2.
`
docker build -t us.icr.io/$MY_NAMESPACE/guestbook:v2 . && docker push us.icr.io/$MY_NAMESPACE/guestbook:v2
`

3.
`
ibmcloud cr images
`

4.
`
kubectl set image deployment/guestbook guestbook=us.icr.io/$MY_NAMESPACE/guestbook:v2
`

5.
`
kubectl rollout status deployment/guestbook
`

6.
`
kubectl get deployments -o wide
`

7.
`
curl -L localhost:8001/api/v1/namespaces/sn-labs-$USERNAME/services/guestbook/proxy
`

8.
`
kubectl rollout undo deployment/guestbook
`

9.
`
kubectl rollout status deployment/guestbook
`

10.
`
kubectl get deployments -o wide
`

11.
`
curl -L localhost:8001/api/v1/namespaces/sn-labs-$USERNAME/services/guestbook/proxy
`